document.addEventListener('DOMContentLoaded', () => {
    
    let items = document.getElementsByClassName('item')
    let pinkTarget = document.getElementsByClassName('target pink')[0].getElementsByTagName('span')[0]
    let redTarget = document.getElementsByClassName('target red')[0].getElementsByTagName('span')[0]
    
    for (let i = 0; i < items.length; i++) {
       
       let item = items[i].childNodes[3].innerText
 
       if (item == "cible 1") {
          items[i].classList.remove('light-grey')
          items[i].classList.add('pink')          
          pinkTarget.innerText = i - 1
       }
       
       if (item == "cible 2") {
          items[i].classList.remove('light-grey')
          items[i].classList.add('red')
          redTarget.innerText = i - 1
       }
     
    }
})