document.addEventListener('DOMContentLoaded', () => {
 
   let divContent = document.createElement('div');
   divContent.id = 'content';
 
   document.body.appendChild(divContent);
 
   let divMenuDarkGrey = document.createElement('div');
   divMenuDarkGrey.className = 'menu dark-grey';
 
   divContent.appendChild(divMenuDarkGrey);
 
   let divItemLightGrey = document.createElement('div');
   divItemLightGrey.className = 'item light-grey';
 
   let divItemLightGrey_h2 = document.createElement('h2');
   let divItemLightGrey_span = document.createElement('span')
 
   divItemLightGrey.appendChild(divItemLightGrey_h2);
   divItemLightGrey.appendChild(divItemLightGrey_span);
 
   for (let i = 0; i < 5; i++) {
     let clone = divItemLightGrey.cloneNode(true);
     divMenuDarkGrey.appendChild(clone);
   }
   
   let h2 = document.getElementsByTagName('h2');
   for (let j = 0; j < 5; j++) {
     h2[j].innerText = j+1; 
   }
   let span = document.getElementsByTagName('span');
 
   span[0].innerText = 'cible 1'; 
   span[1].innerText = 'cible 2'; 
   span[2].innerText = 'cible 2'; 
   span[3].innerText = 'cible 1'; 
   span[4].innerText = 'cible 2'; 
 
   let divTargetsGrey = document.createElement('div');
   divTargetsGrey.className = 'targets grey';
 
   divContent.appendChild(divTargetsGrey);
 
   let divTargetPink = document.createElement('div');
   divTargetPink.className = 'target pink';
   let divTargetRed = document.createElement('div');
   divTargetRed.className = 'target red';
 
   divTargetsGrey.appendChild(divTargetPink);
   divTargetsGrey.appendChild(divTargetRed);
 
   let divTargetPink_h3 = document.createElement('h3');
   divTargetPink_h3.innerText = 'cible 1';
 
   let divTargetRed_h3 = document.createElement('h3');
   divTargetRed_h3.innerText = 'cible 2';
 
   let divTarget_span = document.createElement('span');
   divTarget_span.className = 'compteur';
   divTarget_span.innerText = 0;
 
   divTargetPink.appendChild(divTargetPink_h3);
   divTargetPink.appendChild(divTarget_span);
   divTargetRed.appendChild(divTargetRed_h3);
   divTargetRed.appendChild(divTarget_span.cloneNode(true));

   let items = document.getElementsByClassName('item')
   let pinkTarget = document.getElementsByClassName('target pink')[0].getElementsByTagName('span')[0]
   let redTarget = document.getElementsByClassName('target red')[0].getElementsByTagName('span')[0]
    
    for (let i = 0; i < items.length; i++) {
             
      let item = items[i].childNodes[1].innerText
      
      if (item == "cible 1") {
         items[i].classList.remove('light-grey')
         items[i].classList.add('pink')          
         pinkTarget.innerText = i - 1
      }
      
      if (item == "cible 2") {
         items[i].classList.remove('light-grey')
         items[i].classList.add('red')
         redTarget.innerText = i - 1
      }
     
    }
 
 })
 