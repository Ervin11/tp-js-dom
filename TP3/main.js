const DRAGGABLE_ELEMENTS_WIDTH = 100
const DRAGGABLE_ELEMENTS_HEIGHT = 100

let currentContentWidth = null
let currentContentHeight = null

document.addEventListener('DOMContentLoaded', () => {
   onResize() // to initialize currentContentWidth / currentContentHeight
   renderDraggableElements()
   attachDragEvents()
})

function attachDragEvents() {

    //-- Exercice principal : Implémentez le drag and drop
    //-- Exercice bonus 1 : la dernière box relachée doit être au dessus des autres
    //-- Exercice bonus 2 : lorsque deux box sont en contact, elles doivent être teintes en rouge

    let draggableBoxes = document.getElementsByClassName('draggableBox')
   
    for (let i = 0; i < draggableBoxes.length; i++) {
        
        draggableBoxes[i].setAttribute('draggable', true)
        draggableBoxes[i].addEventListener('mousedown', function(e) {

            this.style.position = 'absolute'
            this.style.zIndex = 1000;

            function moveAt(pageX, pageY) {
                
                draggableBoxes[i].style.left = pageX - draggableBoxes[i].offsetWidth / 2 + 'px';
                draggableBoxes[i].style.top = pageY - draggableBoxes[i].offsetHeight / 2 + 'px';
            }                                                    

            function onMouseMove(e) {
                moveAt(e.pageX, e.pageY);
            }
                    
            this.addEventListener('mousemove', onMouseMove);
                    
            this.addEventListener('mouseup', function() {
                draggableBoxes[i].removeEventListener('mousemove', onMouseMove);                    
            })          

            this.ondragstart = function() {
                return false;
            };
        })
            
    }
      
}

function renderDraggableElements() {
   const contentElement = document.getElementById('content')
   const maxLeft = currentContentWidth - DRAGGABLE_ELEMENTS_WIDTH
   const maxTop = currentContentHeight - DRAGGABLE_ELEMENTS_HEIGHT
   
   for (let i = 0; i <= 10; i++) {
      const divElement = document.createElement('div')
      divElement.className = 'draggableBox'
      divElement.appendChild(document.createTextNode(`Box nº${i}`))
      divElement.style.left = Math.floor(Math.random() * maxLeft) + 'px'
      divElement.style.top = Math.floor(Math.random() * maxTop) + 'px'
      contentElement.appendChild(divElement)
   }
}

window.addEventListener('optimizedResize', onResize)

function onResize() {
   const contentElement = document.getElementById('content')
   
   //-- Exercice Bonus 3: implémenter ici le repositionnement des box lorsque la fenêtre change de taille, les box doivent proportionnellement se retrouver à la même place
   
   currentContentWidth = contentElement.offsetWidth
   currentContentHeight = contentElement.offsetHeight
}

// See https://developer.mozilla.org/en-US/docs/Web/Events/resize
// Prevent resize event to be fired way too often, this means neither lags nor freezes
{
   function throttle(type, name, obj = window) {
      let running = false
      const event = new CustomEvent(name)
      obj.addEventListener(type, () => {
         if (running) return
         running = true
         requestAnimationFrame(() => {
            obj.dispatchEvent(event)
            running = false
         })
      })
   }

   /* init - you can init any event */
   throttle('resize', 'optimizedResize');
}